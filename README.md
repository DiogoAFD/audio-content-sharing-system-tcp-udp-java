# **Audio Content Sharing System** #
###  ###
In this project an application was developed using TCP sockets for communication between servers and between servers and clients. It was also used UDP sockets for communication between clients. This application allows you to send music between clients. Each client, through the interface, can register with a username and a password that will be used in the login.
After the login clients can send a request of a song to their server or can announce that they are available to send songs to other clients who might need.